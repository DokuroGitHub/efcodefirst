﻿using AutoMapper;
using EFCodeFirst.Models;
using EFCodeFirst.Models.DTO;

namespace EFCodeFirst;

public class MappingConfig : Profile
{
    public MappingConfig()
    {
        CreateMap<Grade, GradeDTO>().ReverseMap();
        CreateMap<Grade, GradeCreateDTO>().ReverseMap();
        CreateMap<Grade, GradeUpdateDTO>().ReverseMap();
    }
}