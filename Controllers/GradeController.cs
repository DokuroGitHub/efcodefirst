﻿using EFCodeFirst.Models;
using EFCodeFirst.Models.DTO;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using AutoMapper;
using EFCodeFirst.Repository.IRepository;
using System.Net;
using EFCodeFirst.Logging;

namespace EFCodeFirst.Controllers;

[ApiController]
[Route("/api/[controller]")]
public class GradeController : ControllerBase
{

    private readonly ICustomLogger _logger;
    private readonly IMapper _mapper;
    private readonly IGradeRepository _dbGrade;

    private APIResponse _response;

    public GradeController(ICustomLogger logger, IMapper mapper, IGradeRepository dbGrade)
    {
        _logger = logger;
        _mapper = mapper;
        _dbGrade = dbGrade;
        _response = new();
    }

    [HttpGet(Name = "GetAll[controller]")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    public async Task<ActionResult> GetAll()
    {
        try
        {
            IEnumerable<Grade> gradeList = await _dbGrade.GetAllAsync();
            _response.Result = _mapper.Map<List<GradeDTO>>(gradeList);
            _response.StatusCode = HttpStatusCode.OK;
            _response.IsSuccess = true;
        }
        catch (Exception ex)
        {
            _logger.Log($"GetAll, err: {ex}", "");
            _response.StatusCode = HttpStatusCode.InternalServerError;
            _response.ErrorMessages = new List<string>() {
                    ex.ToString()
                };
            _response.IsSuccess = false;
            return BadRequest(_response);
        }
        return Ok(_response);
    }

    [HttpGet("{id:int}", Name = "GetGrade")]
    [ProducesResponseType(typeof(GradeDTO), StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<ActionResult<APIResponse>> Get(int id)
    {
        try
        {
            if (id == 0)
            {
                _logger.Log($"Get error with id: {id}", "error");
                _response.StatusCode = HttpStatusCode.BadRequest;
                _response.IsSuccess = false;
                _response.ErrorMessages = new List<string>() {
                        $"Get error with id: {id}, BadRequest"
                    };
                return BadRequest(_response);
            }
            var grade = await _dbGrade.GetAsync(x => x.GradeId == id);
            if (grade == null)
            {
                _logger.Log($"Get error with id: {id}", "error");
                _response.StatusCode = HttpStatusCode.NotFound;
                _response.IsSuccess = false;
                _response.ErrorMessages = new List<string>() {
                        $"Get error with id: {id}, NotFound"
                    };
                return NotFound(_response);
            }

            _response.Result = _mapper.Map<GradeDTO>(grade);
            _response.StatusCode = HttpStatusCode.OK;
            _response.IsSuccess = true;
        }
        catch (Exception ex)
        {
            _logger.Log($"Get, err: {ex}", "");
            _response.StatusCode = HttpStatusCode.InternalServerError;
            _response.ErrorMessages = new List<string>() {
                    ex.ToString()
                };
            _response.IsSuccess = false;
            return BadRequest(_response);
        }
        return Ok(_response);
    }

    [HttpPost(Name = "Create[controller]")]
    [ProducesResponseType(StatusCodes.Status201Created)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public async Task<ActionResult<APIResponse>> Create([FromBody] GradeCreateDTO createDTO)
    {
        try
        {
            if (!ModelState.IsValid || createDTO == null)
            {
                ModelState.AddModelError("CustomError", "Model can not be null");
                _logger.Log("Create error: Model can not be null", "error");
                _response.StatusCode = HttpStatusCode.BadRequest;
                _response.IsSuccess = false;
                _response.ErrorMessages = new List<string>() {
                        "Create error: Model can not be null, BadRequest"
                    };
                return BadRequest(_response);
            }
            Grade model = _mapper.Map<Grade>(createDTO);
            model.CreatedAt = DateTime.Now;
            await _dbGrade.CreateAsync(model);

            _response.Result = _mapper.Map<GradeDTO>(model);
            _response.StatusCode = HttpStatusCode.Created;
            return CreatedAtRoute("GetGrade", new { id = model.GradeId }, _response);
        }
        catch (Exception ex)
        {
            _logger.Log($"Create, err: {ex}", "");
            _response.StatusCode = HttpStatusCode.InternalServerError;
            _response.ErrorMessages = new List<string>() {
                    ex.ToString()
                };
            _response.IsSuccess = false;
            return BadRequest(_response);
        }
    }

    [HttpDelete("{id:int}", Name = "Delete[controller]")]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    public async Task<ActionResult<APIResponse>> Delete(int id)
    {
        try
        {
            if (id == 0)
            {
                _logger.Log($"Delete error with id: {id}", "error");
                _response.StatusCode = HttpStatusCode.BadRequest;
                _response.IsSuccess = false;
                _response.ErrorMessages = new List<string>() {
                        $"Delete error with id: {id}, BadRequest"
                    };
                return BadRequest(_response);
            }
            var grade = await _dbGrade.GetAsync(x => x.GradeId == id);
            if (grade == null)
            {
                _logger.Log($"Delete error with id: {id}", "error");
                _response.StatusCode = HttpStatusCode.NotFound;
                _response.IsSuccess = false;
                _response.ErrorMessages = new List<string>() {
                        $"Delete error with id: {id}, NotFound"
                    };
                return NotFound(_response);
            }
            await _dbGrade.RemoveAsync(grade);

            _response.StatusCode = HttpStatusCode.NoContent;
            _response.IsSuccess = true;
        }
        catch (Exception ex)
        {
            _logger.Log($"Delete, err: {ex}", "");
            _response.StatusCode = HttpStatusCode.InternalServerError;
            _response.ErrorMessages = new List<string>() {
                    ex.ToString()
                };
            _response.IsSuccess = false;
            return BadRequest(_response);
        }
        return Ok(_response);
    }

    [HttpPut("{id:int}", Name = "Update[controller]")]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    public async Task<ActionResult<APIResponse>> Update(int id, [FromBody] GradeUpdateDTO updateDTO)
    {
        try
        {
            if (updateDTO == null)
            {
                _logger.Log($"Update error with id: {id}", "error");
                _response.StatusCode = HttpStatusCode.BadRequest;
                _response.IsSuccess = false;
                _response.ErrorMessages = new List<string>() {
                        $"Update error with id: {id}, BadRequest"
                    };
                return BadRequest(_response);
            }

            Grade model = _mapper.Map<Grade>(updateDTO);

            await _dbGrade.UpdateAsync(model);

            _response.StatusCode = HttpStatusCode.NoContent;
            _response.IsSuccess = true;
        }
        catch (Exception ex)
        {
            _logger.Log($"Update, err: {ex}", "");
            _response.StatusCode = HttpStatusCode.InternalServerError;
            _response.ErrorMessages = new List<string>() {
                    ex.ToString()
                };
            _response.IsSuccess = false;
            return BadRequest(_response);
        }

        return Ok(_response);
    }

    [HttpPatch("{id:int}", Name = "UpdatePartial[controller]")]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    public async Task<IActionResult> UpdatePartial(int id, JsonPatchDocument<GradeUpdateDTO> patchDTO)
    {
        try
        {
            if (patchDTO == null || id == 0)
            {
                _logger.Log($"UpdatePartial error with id: {id}", "error");
                _response.StatusCode = HttpStatusCode.BadRequest;
                _response.IsSuccess = false;
                _response.ErrorMessages = new List<string>() {
                        $"UpdatePartial error with id: {id}, BadRequest"
                    };
                return BadRequest(_response);
            }
            if (!ModelState.IsValid)
            {
                _logger.Log($"UpdatePartial error with id: {id}", "error");
                _response.StatusCode = HttpStatusCode.BadRequest;
                _response.IsSuccess = false;
                _response.ErrorMessages = new List<string>() {
                        $"UpdatePartial error with id: {id}, BadRequest"
                    };
                _response.Result = ModelState;
                return BadRequest(_response);
            }

            var grade = await _dbGrade.GetAsync(x => x.GradeId == id, tracked: false);
            if (grade == null)
            {
                _logger.Log($"UpdatePartial error with id: {id}", "error");
                _response.StatusCode = HttpStatusCode.BadRequest;
                _response.IsSuccess = false;
                _response.ErrorMessages = new List<string>() {
                        $"UpdatePartial error with id: {id}, NotFound"
                    };
                return NotFound(_response);
            }
            GradeUpdateDTO updateDTO = _mapper.Map<GradeUpdateDTO>(grade);
            patchDTO.ApplyTo(updateDTO, ModelState);
            Grade model = _mapper.Map<Grade>(updateDTO);

            await _dbGrade.UpdateAsync(model);

            _response.StatusCode = HttpStatusCode.NoContent;
            _response.IsSuccess = true;
        }
        catch (Exception ex)
        {
            _logger.Log($"UpdatePartial, err: {ex}", "");
            _response.StatusCode = HttpStatusCode.InternalServerError;
            _response.ErrorMessages = new List<string>() {
                    ex.ToString()
                };
            _response.IsSuccess = false;
            return BadRequest(_response);
        }

        return Ok(_response);
    }
}