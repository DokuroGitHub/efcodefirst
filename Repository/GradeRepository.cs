﻿using EFCodeFirst.Data;
using EFCodeFirst.Models;
using EFCodeFirst.Repository.IRepository;

namespace EFCodeFirst.Repository;

public class GradeRepository : Repository<Grade>, IGradeRepository
{
    private readonly MyDbContext _db;

    public GradeRepository(MyDbContext db) : base(db)
    {
        _db = db;
    }

    public async Task<Grade> UpdateAsync(Grade entity)
    {
        entity.UpdatedAt = DateTime.Now;
        _db.Update(entity);
        await _db.SaveChangesAsync();
        return entity;
    }
}