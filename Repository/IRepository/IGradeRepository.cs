﻿using EFCodeFirst.Models;

namespace EFCodeFirst.Repository.IRepository;

public interface IGradeRepository : IRepository<Grade>
{
    Task<Grade> UpdateAsync(Grade entity);
}