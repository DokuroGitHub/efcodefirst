﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace EFCodeFirst.Models;

public class Grade
{
    public Grade()
    {
        GradeName = "";
        Section = "";
        Students = new HashSet<Student>();
    }

    public int GradeId { get; set; }
    public string GradeName { get; set; }
    public string Section { get; set; }
    public DateTime CreatedAt { get; set; }
    public DateTime UpdatedAt { get; set; }

    [XmlIgnore]
    [IgnoreDataMember]
    public virtual ICollection<Student> Students { get; set; }
}