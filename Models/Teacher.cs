﻿namespace EFCodeFirst.Models;

public class Teacher
{
    public Teacher()
    {
        TeacherName = "";
        ModeOfTeaching = TeachingMode.ClassRoom;
    }

    public int Id { get; set; }
    public string TeacherName { get; set; }
    public TeachingMode ModeOfTeaching { get; set; }
}

public enum TeachingMode
{
    Online,
    ClassRoom,
    LiveOnline
}