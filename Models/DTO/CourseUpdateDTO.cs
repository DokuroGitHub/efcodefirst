﻿namespace EFCodeFirst.Models.DTO;

public class CourseUpdateDTO
{
    public CourseUpdateDTO()
    {
        CourseName = "";
        Students = new HashSet<StudentDTO>();
    }

    public int CourseId { get; set; }
    public string CourseName { get; set; }

    public virtual ICollection<StudentDTO> Students { get; set; }
}