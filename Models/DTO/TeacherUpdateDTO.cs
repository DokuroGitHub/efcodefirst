﻿namespace EFCodeFirst.Models.DTO;

public class TeacherUpdateDTO
{
    public TeacherUpdateDTO()
    {
        TeacherName = "";
        ModeOfTeaching = TeachingMode.ClassRoom;
    }

    public int Id { get; set; }
    public string TeacherName { get; set; }
    public TeachingMode ModeOfTeaching { get; set; }
}