﻿using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace EFCodeFirst.Models.DTO;

public class CourseCreateDTO
{
    public CourseCreateDTO()
    {
        CourseName = "";
        Students = new HashSet<StudentDTO>();
    }

    public int CourseId { get; set; }
    public string CourseName { get; set; }

    [XmlIgnore]
    [IgnoreDataMember]
    public virtual ICollection<StudentDTO> Students { get; set; }
}