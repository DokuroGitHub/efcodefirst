﻿namespace EFCodeFirst.Models.DTO;

public class TeacherDTO
{
    public TeacherDTO()
    {
        TeacherName = "";
        ModeOfTeaching = TeachingMode.ClassRoom;
    }

    public int Id { get; set; }
    public string TeacherName { get; set; }
    public TeachingMode ModeOfTeaching { get; set; }
}