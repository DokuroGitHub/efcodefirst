﻿namespace EFCodeFirst.Models.DTO;

public class CourseDTO
{
    public CourseDTO()
    {
        CourseName = "";
        Students = new HashSet<StudentDTO>();
    }
    
    public int CourseId { get; set; }
    public string CourseName { get; set; }

    public virtual ICollection<StudentDTO> Students { get; set; }
}