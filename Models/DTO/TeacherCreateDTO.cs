﻿namespace EFCodeFirst.Models.DTO;

public class TeacherCreateDTO
{
    public TeacherCreateDTO()
    {
        TeacherName = "";
        ModeOfTeaching = TeachingMode.ClassRoom;
    }

    public int Id { get; set; }
    public string TeacherName { get; set; }
    public TeachingMode ModeOfTeaching { get; set; }
}