﻿namespace EFCodeFirst.Models.DTO;

public class StudentUpdateDTO
{
    public StudentUpdateDTO()
    {
        StudentName = "";
        RowVersion = new byte[] { };
        Grade = new GradeDTO();
        Address = new StudentAddressDTO();
        Courses = new HashSet<CourseDTO>();
    }

    public int StudentID { get; set; }
    public string StudentName { get; set; }
    public DateTime? DateOfBirth { get; set; }
    public decimal Height { get; set; }
    public float Weight { get; set; }
    public byte[] RowVersion { get; set; }

    //fully defined relationship
    public int? GradeId { get; set; }

    public virtual GradeDTO Grade { get; set; }

    public virtual StudentAddressDTO Address { get; set; }
    public virtual ICollection<CourseDTO> Courses { get; set; }
}