﻿using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace EFCodeFirst.Models.DTO;

public class GradeCreateDTO
{
    public GradeCreateDTO()
    {
        GradeName = "";
        Section = "";
        Students = new HashSet<StudentDTO>();
    }
    public int GradeId { get; set; }
    public string GradeName { get; set; }
    public string Section { get; set; }
    public DateTime CreatedAt { get; set; }
    public DateTime UpdatedAt { get; set; }

    [XmlIgnore]
    [IgnoreDataMember]
    public virtual ICollection<StudentDTO> Students { get; set; }
}