﻿using System.Collections.Generic;

namespace EFCodeFirst.Models;

public class Course
{
    public Course()
    {
        CourseName = "";
        Students = new HashSet<Student>();
    }

    public int CourseId { get; set; }
    public string CourseName { get; set; }

    public virtual ICollection<Student> Students { get; set; }
}