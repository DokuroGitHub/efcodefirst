using EFCodeFirst.Models;

namespace EFCodeFirst.Data;
public static class DbInitializer
{
    public static void Seed(MyDbContext context)
    {
        if (!(context.Grades?.Any() ?? true))
        {
            context.Grades.AddRange(
                new Grade
                {
                    GradeId = 1,
                    GradeName = "Grade 1",
                    Section = "A",
                    CreatedAt = DateTime.Now,
                    UpdatedAt = DateTime.Now,
                },
                new Grade
                {
                    GradeId = 2,
                    GradeName = "Grade 1",
                    Section = "B",
                    CreatedAt = DateTime.Now,
                    UpdatedAt = DateTime.Now,
                },
                new Grade
                {
                    GradeId = 3,
                    GradeName = "Grade 1",
                    Section = "C",
                    CreatedAt = DateTime.Now,
                    UpdatedAt = DateTime.Now,
                },
                new Grade
                {
                    GradeId = 4,
                    GradeName = "Grade 2",
                    Section = "A",
                    CreatedAt = DateTime.Now,
                    UpdatedAt = DateTime.Now,
                },
                new Grade
                {
                    GradeId = 5,
                    GradeName = "Grade 3",
                    Section = "A",
                    CreatedAt = DateTime.Now,
                    UpdatedAt = DateTime.Now,
                }
            );

            context.SaveChanges();
        }
    }
}