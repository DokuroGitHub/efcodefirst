﻿using EFCodeFirst.Models;
using Microsoft.EntityFrameworkCore;

namespace EFCodeFirst.Data;

public class MyDbContext : DbContext
{
    public MyDbContext(DbContextOptions<MyDbContext> options)
    : base(options)
    {
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        // Adds configurations for Student from separate class
        modelBuilder.ApplyConfiguration(new StudentConfigurations());

        // 
        modelBuilder.Entity<Teacher>()
            .ToTable("TeacherInfo");

        modelBuilder.Entity<Teacher>()
            .UseTpcMappingStrategy();

        // seed
        DbInitializer.Seed(this);
    }

    public DbSet<Student>? Students { get; set; }
    public DbSet<Grade>? Grades { get; set; }
    public DbSet<Course>? Courses { get; set; }
    public DbSet<StudentAddress>? StudentAddresses { get; set; }
}