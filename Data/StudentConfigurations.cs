﻿using EFCodeFirst.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace EFCodeFirst.Data;

public class StudentConfigurations : IEntityTypeConfiguration<Student>
{
    public void Configure(EntityTypeBuilder<Student> builder)
    {
        builder.ToTable("Student");

        builder.Property(s => s.StudentName)
            .IsRequired()
            .HasMaxLength(50);

        builder.Property(s => s.StudentName)
            .IsConcurrencyToken();

        // Configure a one-to-one relationship between Student & StudentAddress
        builder.HasOne(s => s.Address) // Mark Student.Address property optional (nullable)
            .WithOne(ad => ad.Student) // Mark StudentAddress.Student property as required (NotNull).
            .HasForeignKey<Student>(s => s.StudentID);
    }
}