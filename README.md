# EF-Code-First

Entity Framework Code-First Demo Project

```bash
dotnet add package AutoMapper
dotnet add package AutoMapper.Extensions.Microsoft.DependencyInjection
dotnet add package Microsoft.AspNetCore.JsonPatch
dotnet add package Microsoft.AspNetCore.Mvc.NewtonsoftJson
dotnet add package Microsoft.EntityFrameworkCore
dotnet add package Microsoft.EntityFrameworkCore.SqlServer
dotnet add package Microsoft.EntityFrameworkCore.Tools
dotnet add package ServiceStack.Interfaces
dotnet add package Serilog.AspNetCore
dotnet add package Serilog.Sinks.File

dotnet ef migrations add InitDB
dotnet ef database update
```
