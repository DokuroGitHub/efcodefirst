﻿namespace EFCodeFirst.Logging;

public interface ICustomLogger
{
    void Log(string message, string type);
}