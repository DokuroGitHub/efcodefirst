﻿namespace EFCodeFirst.Logging;

public class CustomLoggerV2 : ICustomLogger
{
    public void Log(string message, string type)
    {
        if (type == "error")
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("ERROR - " + message);
            Console.ResetColor();
        }
        else if (type == "warning")
        {
            Console.ForegroundColor = ConsoleColor.DarkYellow;
            Console.WriteLine("WARNING - " + message);
            Console.ResetColor();
        }
        else
        {
            Console.WriteLine(message);
        }
    }
}