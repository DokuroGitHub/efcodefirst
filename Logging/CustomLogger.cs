﻿namespace EFCodeFirst.Logging;

public class CustomLogger : ICustomLogger
{
    public void Log(string message, string type)
    {
        if (type == "error")
        {
            Console.WriteLine("ERROR - " + message);
        }
        else if (type == "warning")
        {
            Console.WriteLine("WARNING - " + message);
        }
        else
        {
            Console.WriteLine(message);
        }
    }
}